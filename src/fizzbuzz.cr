require "./fizzbuzz_methods"

class Fizzbuzz
  property number : Int32

  def initialize(number : Int32)
    @number = number
  end

  include FizzbuzzMethods
end
