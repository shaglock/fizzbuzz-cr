require "./spec_helper"

describe Fizzbuzz do
  it "is created with correct value" do
    object = create_fizzbuzz_object(20)
    object.should_not be_nil
    object.number.should eq(20)
  end
end
